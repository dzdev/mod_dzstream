<?php
/**
 * @version     1.0.0
 * @package     mod_dzstream
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */

// no direct access
defined('_JEXEC') or die;
JHtml::_('jquery.framework');
JFactory::getDocument()->addStyleSheet(JUri::root() . 'components/com_dzstream/assets/css/flags.css');
JFactory::getDocument()->addScript(JUri::root() . 'components/com_dzstream/assets/js/channel.js');
?>
<table data-stream-live>
    <tbody>
        <?php foreach ($items as $item) : ?>
        <tr data-stream-id="<?= $item->name; ?>" data-stream-platform="<?= $item->platform; ?>">
            <td width="35" align="left"><img src="<?= JUri::root() . 'components/com_dzstream/assets/img/' . $item->platform. '.ico'; ?>" alt="<?php echo $item->platform;?>" /></td>
            <td>
                <a href="<?= $item->channel->getLink(); ?>" target="_blank" title="<?php echo $item->platform;?> | <?= $item->title; ?>">
                    <?= $item->title; ?>
                </a>
            </td>
            <td align="right"><small data-stream-attrib="is_live" data-stream-attrib-transform='{"true": "LIVE", "false": "OFFLINE", "null": "-"}'></small></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<style type="text/css">
    .channels .module-content{padding:5px 10px}
    .channels table{border:none;color:#999;width:100%;}
    .channels img{border:1px solid #454545;padding:4px;height:24px;width:24px;}
    .channels tr td{border-bottom:1px solid #333;padding:5px 0px;}
    .channels tr:last-child td{border:none;}
</style>
