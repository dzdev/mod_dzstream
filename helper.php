<?php
/**
 * @version     1.0.0
 * @package     mod_dzstream
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_dzstream/models/');

abstract class modDZStreamHelper
{
    public static function getList($params) {
        $model = JModelLegacy::getInstance('Channels', 'DZStreamModel', array('ignore_request' => true));

        // Set application parameters in model
        $app = JFactory::getApplication();
        $appParams = $app->getParams();
        $model->setState('params', $appParams);

        // Set the filters based on the module params
        $model->setState('list.start', 0);
        $model->setState('list.limit', (int) $params->get('display_num', 10));

        $model->setState('filter.catid', $params->get('catid', 0, 'int'));

        // Ordering
        $model->setState('list.ordering', $params->get('ordering', 'a.ordering'));
        $model->setState('list.direction', $params->get('direction', 'ASC'));

        // Filter by featured
        $model->setState('list.featured', $params->get('featured', 1));

        // Filter by language
        $model->setState('filter.language', $params->get('language'));

        // Filter by platform
        $model->setState('filter.platform', $params->get('platform'));

        $items = $model->getItems();

        return $items;
    }
}
